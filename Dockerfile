FROM debian:stretch as build_image

RUN apt-get update \
    && apt-get install -y curl \
    && curl -SL https://github.com/andreas-schroeder/kafka-health-check/releases/download/v0.1.0/kafka-health-check_0.1.0_linux_amd64.tar.gz \
        | tar -zx -C /usr/local/bin \
    && chown root:root /usr/local/bin/kafka-health-check \
    && chmod 755 /usr/local/bin/kafka-health-check

FROM debian:stretch
COPY --from=build_image /usr/local/bin/kafka-health-check /usr/local/bin/kafka-health-check
